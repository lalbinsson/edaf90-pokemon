import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { Subject } from 'rxjs';
import { TestpokemonService } from './testpokemon.service';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  list = [];
  private subject = new Subject<any[]>();
  public todo$ = this.subject.asObservable().pipe(
    shareReplay()
  );

  constructor(private testpokemonService: TestpokemonService) { 
    this.testpokemonService.getBattlePokemon("")
    .then((myJson) => {
     this.addPokemon(myJson.cards[0]);
    
     });
  }

  addPokemon(todo){
    this.list = [...this.list, {...todo}];
    this.subject.next(this.list);
  }

  getPokemon(): Observable<any[]> {
    return this.todo$;
  }

  
}
