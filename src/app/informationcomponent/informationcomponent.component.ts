import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import {ActivatedRoute} from '@angular/router';
import { TestpokemonService } from '../testpokemon.service';



@Component({
  selector: 'app-informationcomponent',
  templateUrl: './informationcomponent.component.html',
  styleUrls: ['./informationcomponent.component.css']
})
export class InformationcomponentComponent implements OnInit {
  pokemon: [];
  id: any;
  testpokemons: any;

  constructor(private appService: AppService, private activatedroute: ActivatedRoute, private testpokemonService: TestpokemonService) {
    this.activatedroute.queryParams.subscribe(data => {
      this.id=data.id;
    })
   }
  ngOnInit(): void {
    this.testpokemons=this.testpokemonService.getPokemonById(this.id);
    console.log(this.testpokemons);
    
  }

}
