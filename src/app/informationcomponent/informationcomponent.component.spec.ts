import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationcomponentComponent } from './informationcomponent.component';

describe('InformationcomponentComponent', () => {
  let component: InformationcomponentComponent;
  let fixture: ComponentFixture<InformationcomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationcomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
