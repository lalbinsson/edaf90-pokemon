import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { TestpokemonService } from '../testpokemon.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-searchcomponent',
  templateUrl: './searchcomponent.component.html',
  styleUrls: ['./searchcomponent.component.css']
})
export class SearchcomponentComponent implements OnInit {

  constructor(private testpokemonService: TestpokemonService, private messageService: MessageService, private appService: AppService) {

   }

  testpokemons: any[];

  selectedTestpokemon: any;


  getPokemons(): void {
    let list =this.testpokemonService.getTestpokemons("");
    this.testpokemons = list;
  }

  search(event: any){
    let name =event.target.value;   
    this.testpokemons = this.testpokemonService.getTestpokemons(name);
  }

  onSelect(testpokemon: any): void {
    this.selectedTestpokemon = testpokemon; 
    this.messageService.add(`TestpokemonService: Selected pokemon id=${testpokemon.id}`);
  }
 

  ngOnInit(): void {
    this.getPokemons();
  }
}
