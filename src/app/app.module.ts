import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartpageComponent } from './startpage/startpage.component';
import { TestpageComponent } from './testpage/testpage.component';
import { SearchcomponentComponent } from './searchcomponent/searchcomponent.component';
import { PokeballcomponentComponent } from './pokeballcomponent/pokeballcomponent.component';
import { BattlefieldcomponentComponent } from './battlefieldcomponent/battlefieldcomponent.component';
import { InformationcomponentComponent } from './informationcomponent/informationcomponent.component';

import { HttpClientModule }    from '@angular/common/http';
import { MessagesComponent } from './messages/messages.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const appRoutes: Routes = [
  { path: '', component: StartpageComponent },
  { path: 'testpage', component: TestpageComponent },
  { path: 'searchpage', component: SearchcomponentComponent },
  { path: 'pokeball', component: PokeballcomponentComponent },
  { path: 'battlefield', component: BattlefieldcomponentComponent },
  { path: 'information', component: InformationcomponentComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    StartpageComponent,
    TestpageComponent,
    SearchcomponentComponent,
    PokeballcomponentComponent,
    BattlefieldcomponentComponent,
    InformationcomponentComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
{enableTracing: true }
),
AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }