import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { promise } from 'protractor';
import { createReadStream } from 'fs';

@Injectable({
  providedIn: 'root'
})
export class TestpokemonService {


  constructor(private messageService: MessageService) { }

  private pokemonsUrl = 'https://api.pokemontcg.io/v1' // Url

/** Log a HeroService message with the MessageService */
private log(message: string) {
  this.messageService.add(`TestpokemonService: ${message}`);
}

/**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

/* Fetching data to informationcomponent */
getPokemonById(id){
  let list = [];

 fetch(this.pokemonsUrl+"/cards/"+id)
 .then((response) => {
   return response.json();
 })
 .then((myJson) => {
   Promise.all(Object.values(myJson).map(obj =>{
       list.push(obj);
   }))
 });
 return list;
}

/* Fetching pokemons to searchpage*/
  getTestpokemons(name): any{ 
    let list = [];

  fetch(this.pokemonsUrl+"/cards?name="+name)
  .then((response) => {
    return response.json();
  })
  .then((myJson) => {
  
    Promise.all(Object.values(myJson).map(obj =>{
      Object.values(obj).map(o =>
        {
          list.push(o);
                })
    }))
  });
  return list;
  }


  getAllTestPokemons(name): any{ //: Observable<Testpokemon[]> 

    let list = [];

    fetch(this.pokemonsUrl+"/cards?name="+name+"&supertype=Pokémon")
  .then((response) => {
    return response.json();
  })
  .then((myJson) => {
  
    Promise.all(Object.values(myJson).map(obj =>{
      Object.values(obj).map(o =>
        {
          list.push(o);
                })
    }))
  });
  return list;


  }


  /* Fetching pokemons to testpage*/
    getFilteredpokemons(element, basic, rarity): any{ 
      let list = [];
      console.log(element, basic, rarity);

    return fetch(this.pokemonsUrl+"/cards?types="+element+"&subtype="+basic+"&rarity="+rarity)
    .then((response) => {
      return response.json();
    });
  }

  /* Fetching Pokemons to app.service, used for battlefield and pokeball*/
  getBattlePokemon(name): any{
    let list = {pokes: []};
    let arraylist = [];

    return fetch(this.pokemonsUrl+"/cards?name="+name +"&supertype=Pokémon")
    .then((response) => {
      return response.json();
    });
  }

}
