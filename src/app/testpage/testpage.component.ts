import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { TestpokemonService } from '../testpokemon.service';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import { finished } from 'stream';

@Component({
  selector: 'app-testpage',
  templateUrl: './testpage.component.html',
  styleUrls: ['./testpage.component.css']
})

export class TestpageComponent implements OnInit {
  

  constructor(private testpokemonService: TestpokemonService, private messageService: MessageService, fb: FormBuilder) {
    this.pokeForm = fb.group({
      element: new FormBuilder(),
      basic: new FormBuilder(),
      rarity: new FormBuilder()
    });

  }

  pokeForm: FormGroup;

  testpokemons: any;

  defaultPokemons: any;

  filteredPokemons: any;

  myPokemon : any;

  types = ["Colorless","Darkness","Dragon","Fairy","Fighting","Fire","Grass","Lightning","Metal","Psychic","Water"]; //läsa in den här från API:et istället.

  feedback = "";

  generatePokemon() {
    this.defaultPokemons =  this.testpokemons.filter(pokemon => {
      return pokemon.types[0] == this.pokeForm.value.element;
    });
    let temp = this.defaultPokemons.filter(pokemon => {
      return pokemon.subtype === this.getBasic(this.pokeForm.value.basic);
    })
    .filter(pokemon => {
      return pokemon.rarity === this.getRarity(this.pokeForm.value.rarity);
    });
    this.filteredPokemons = temp;
  }

  getRarity(rarity){
    if(rarity == 0 ){
      return "Common"
    } else if(rarity == 1 ){
      return "Uncommon"
    } else if(rarity == 2 ){
      return "Rare"
    } else {
      console.log("error rarity level");
    }
  }


  getBasic(basic){
    if (basic === 0) {
      return "Basic";
    } else if (basic === 1) {
      return "Stage 1"
    } else if (basic === 2) {
      return "Stage 2"
    } else if (basic === 3) {
      return "Restored"
    } else if (basic === 4) {
      return "GX"
    } else if (basic === 5) {
      return "Break"
    } else if (basic === 6) {
      return "EX"
    } else if (basic === 7) {
      return "Legend"
    } else {
      console.log("error basic level");
    }

  }

  getRandom() {
    let length = this.filteredPokemons.length;
    if(length !== 0) {
      let rand = Math.floor(Math.random()*length);
      this.myPokemon = this.filteredPokemons[rand];
    }
    else {
      let length = this.defaultPokemons.length;
      if(length !== 0){
      let rand = Math.floor(Math.random()*length);
      this.myPokemon = this.defaultPokemons[rand];
    }
      this.feedback = "";
    }
  }

  onSubmit(): void { //test
    this.generatePokemon();
    this.getRandom();
  }

  ngOnInit(): void {
    this.myPokemon = "";
    this.testpokemons = this.testpokemonService.getAllTestPokemons("");
  }

}
