import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeballcomponentComponent } from './pokeballcomponent.component';

describe('PokeballcomponentComponent', () => {
  let component: PokeballcomponentComponent;
  let fixture: ComponentFixture<PokeballcomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokeballcomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeballcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
