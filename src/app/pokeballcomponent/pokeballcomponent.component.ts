import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { TestpokemonService } from '../testpokemon.service';
import { AppService } from '../app.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-pokeballcomponent',
  templateUrl: './pokeballcomponent.component.html',
  styleUrls: ['./pokeballcomponent.component.css']
})
export class PokeballcomponentComponent implements OnInit {

  testpokemons: any[];
  list$ = this._alertService.getPokemon();
  sub;

  constructor(private testpokemonService: TestpokemonService, private messageService: MessageService, private _alertService: AppService) {
   }


  ngOnInit(): void {
    this.sub = this.list$.subscribe(data => {
      this.testpokemons = data;
    });
    
  }
  ngOnDestroy(){
    this.sub.unsubscribe();
    
  }

}
