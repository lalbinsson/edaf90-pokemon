import { TestBed } from '@angular/core/testing';

import { TestpokemonService } from './testpokemon.service';

describe('TestpokemonService', () => {
  let service: TestpokemonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestpokemonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
