import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageService } from '../message.service';
import { TestpokemonService } from '../testpokemon.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-battlefieldcomponent',
  templateUrl: './battlefieldcomponent.component.html',
  styleUrls: ['./battlefieldcomponent.component.css']
})
export class BattlefieldcomponentComponent implements OnInit, OnDestroy {

  constructor(private testpokemonService: TestpokemonService, private messageService: MessageService, private _alertService: AppService) { 

  }

  opponents: any;
  opponent: any;
  player: any;
  result = "";
  pokemoninfo = "";
  playerImage = "";
  opponentImage = "";
  disabledOpponent = true;
  disabledPlayer = true;
  isDisabled = true;
  pokemon = "";
  list$ = this._alertService.getPokemon();
  myPokemons = [];
  sub;
  


  getBool(){
    if(this.disabledPlayer){
      return true;
    }else{
      return this.disabledOpponent;
    }
    
  }

  getOpponentPokemons(): void {
     this.testpokemonService.getBattlePokemon("")
     .then((myJson) => {
      this.opponents = myJson;      
      })

  }

  getTheOpponent(event: any): void{
    let temp = this.opponents.cards[Math.floor(Math.random()*100)];
    this.opponent = temp;
    var obj=document.getElementById('image01');
    obj.className = 'show';
    this.opponentImage  = this.opponent.imageUrl;

    this.result = "";
    this.pokemoninfo = "";

    this.disabledOpponent = false;
    this.isDisabled = this.getBool();
  }
  
  startBattle(event: any): any {
    let sumHP = parseInt(this.opponent.hp) + parseInt(this.player.hp)
    let procent = parseInt(this.player.hp) / sumHP;

    const a = (Math.random());
    if (a < procent) {
        console.log("You won!!");
        this.result = "You won!!";
        this.pokemoninfo = "The opponent pokémon is now added to your pokémons";
        this._alertService.addPokemon(this.opponent);      
    } else {
      this.result = "You lost :(";
    }
    this.disabledOpponent = true;
    this.isDisabled = this.getBool();
  }

  onChange(selectedValue){
    this.disabledPlayer = false; 
    this.isDisabled = this.getBool();
    let id = selectedValue;
    for(let pokemon in this.myPokemons){
      var obj = this.myPokemons[pokemon];
      if(id === obj.id){
        this.player = obj;
        this.playerImage = obj.imageUrl;
      }
    }
  }

  ngOnInit(): void {
    
   this.sub=this.list$.subscribe(data =>{
      this.myPokemons = data;
      console.log(this.myPokemons);
    })
    
    this.getOpponentPokemons();
  }

    ngOnDestroy(){
      this.sub.unsubscribe();
      
    }

}
