import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BattlefieldcomponentComponent } from './battlefieldcomponent.component';

describe('BattlefieldcomponentComponent', () => {
  let component: BattlefieldcomponentComponent;
  let fixture: ComponentFixture<BattlefieldcomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BattlefieldcomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BattlefieldcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
